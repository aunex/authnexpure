//
//  AppDelegate.swift
//  AuthnexPodTest
//
//  Created by Himal Madhushan on 6/29/19.
//  Copyright © 2019 Authnex. All rights reserved.
//

import UIKit
import AirshipKit
import Authnex

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let config = UAConfig.default()
        UAirship.takeOff(config)
        UAirship.setLogging(false)
        UAirship.push().userPushNotificationsEnabled = true
        UAirship.push().backgroundPushNotificationsEnabled = true
        if #available(iOS 10.0, *) {
            UAirship.push().defaultPresentationOptions = [.alert, .badge, .sound]
        }
        UAirship.push()?.pushNotificationDelegate = self
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        let rootviewController = RootViewController()
        window?.rootViewController = rootviewController
        
        return true
    }

}

//MARK: Implement push notification deletages
extension AppDelegate: UAPushNotificationDelegate {
    
    func receivedForegroundNotification(_ notificationContent: UANotificationContent, completionHandler: @escaping () -> Void) {
        
        if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            AuthnexCore.sharedInctance.verification(content: notificationContent) { (success, approval , err, code) in
                if success {
                    print("user authenticated")
                    return
                }
                if approval {
                    print("not authenticated, approval push notificatoin, navigate user to approval section")
                    return
                }
                print("user authentication fails : \(err!)")
            }
            completionHandler()
        }
    }
    
    func receivedBackgroundNotification(_ notificationContent: UANotificationContent, completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            AuthnexCore.sharedInctance.verification(content: notificationContent) { (success, approval , err, code) in
                if success {
                    print("user authenticated")
                    return
                }
                if approval {
                    print("not authenticated, approval push notificatoin, navigate user to approval section")
                    return
                }
                print("user authentication fails : \(err!)")
            }
        }
        completionHandler(.noData)
    }
    
    func receivedNotificationResponse(_ notificationResponse: UANotificationResponse, completionHandler: @escaping () -> Void) {
        
        if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            AuthnexCore.sharedInctance.verification(content: notificationResponse.notificationContent) { (success, approval , err, code) in
                if success {
                    print("user authenticated")
                    return
                }
                if approval {
                    print("not authenticated, approval push notificatoin, navigate user to approval section")
                    return
                }
                print("user authentication fails : \(err!)")
            }
        }
        completionHandler()
    }
}

