//
//  RegistrationViewController.swift
//  AuthnexPodTest
//
//  Created by Himal Madhushan on 6/29/19.
//  Copyright © 2019 Authnex. All rights reserved.
//

import UIKit
import Authnex
import AirshipKit

class RegistrationQRController: SharedController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainTitle.text = "QR Registration Example \nSee Code"
        let channelid = UAirship.push()?.channelID
        
        if channelid != nil {
            registerUser(username: "User_Name_Any", companyid: 0, pushtoken: channelid!, mobilenumber: "07xxxxxxx", qrcode: "/02/xiahc2Ecw3AvciOFMKA31hTCMzioSWtOrFExqlWjkMr2gRy9EuwF1egHr5BjPjTYMj5rXgu7o63cqDI2gg==")
        } else {
             UAirship.push()?.registrationDelegate = self
        }
    }
}
//MARK: Push notification registration
extension RegistrationQRController: UARegistrationDelegate {
    func registrationSucceeded(forChannelID channelID: String, deviceToken: String) {
        //MARK: User registration
        if(!AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            
            registerUser(username: "User_Name_Any", companyid: 0, pushtoken: channelID, mobilenumber: "07xxxxxxx", qrcode: "/02/xiahc2Ecw3AvciOFMKA31hTCMzioSWtOrFExqlWjkMr2gRy9EuwF1egHr5BjPjTYMj5rXgu7o63cqDI2gg==")
        }
    }
}

extension RegistrationQRController {
    fileprivate func registerUser(username: String, companyid: Int, pushtoken: String, mobilenumber: String, qrcode: String) {
        AuthnexCore.sharedInctance.userRegistration(userName: username, companyId: companyid, pushToken: pushtoken, mobileNumber: mobilenumber, qrcodeString: qrcode) { (success, err) in
            DispatchQueue.main.async {
                if success {
                    let loginviewcontroller = LoginViewController()
                    self.present(loginviewcontroller, animated: true, completion: nil)
                    return
                }
                
                print("not success \(err!)")
            }
        }
    }
}


