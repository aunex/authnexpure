//
//  ApprovalController.swift
//  AuthnexPodTest
//
//  Created by Himal Madhushan on 6/29/19.
//  Copyright © 2019 Authnex. All rights reserved.
//

import UIKit
import Authnex

class ApprovalController: SharedController {

    override func viewDidLoad() {
        super.viewDidLoad()
        mainTitle.text = "Approval EXample \nSee Code"
        getapprovals()
    }
}

extension ApprovalController {
    fileprivate func getapprovals() {
        AuthnexCore.sharedInctance.approvals(approvalType: "pending") { (approvals, err) in
            if approvals != nil {
                for approve in approvals! {
                    let approval = approve.approvalId
                    print(approval!)
                }
                return
            }
            print(err!)
        }
    }
}
