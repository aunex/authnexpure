//
//  SharedController.swift
//  AuthnexPodTest
//
//  Created by Himal Madhushan on 6/29/19.
//  Copyright © 2019 Authnex. All rights reserved.
//

import UIKit

class SharedController: UIViewController {
    
    let deviceHeight = UIScreen.main.bounds.height
    let deviceWidth = UIScreen.main.bounds.width
    let fontScale = UIScreen.main.bounds.width/375
    public var mainTitle: UILabel!
    public var backbutton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle()
        // Do any additional setup after loading the view.
    }
}

extension SharedController {
    fileprivate func setTitle() {
        view.backgroundColor = .white
        mainTitle = UILabel()
        mainTitle.translatesAutoresizingMaskIntoConstraints = false
        mainTitle.textAlignment = .center
        mainTitle.font = UIFont(name: "Helvetica", size: 25 * fontScale)
        mainTitle.textColor = .black
        mainTitle.numberOfLines = 2
        view.addSubview(mainTitle)
        
        let maintitleConstraints = [mainTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                                    mainTitle.centerYAnchor.constraint(equalTo: view.centerYAnchor)]
        NSLayoutConstraint.activate(maintitleConstraints)
        
        backbutton = UIButton()
        backbutton.translatesAutoresizingMaskIntoConstraints = false
        backbutton.setTitle("Back", for: .normal)
        backbutton.setTitleColor(.black, for: .normal)
        view.addSubview(backbutton)
        
        let backbuttonConstraints = [backbutton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: deviceWidth/18.75),
                                     backbutton.topAnchor.constraint(equalTo: view.topAnchor, constant: deviceHeight/22.23)]
        NSLayoutConstraint.activate(backbuttonConstraints)
        view.bringSubviewToFront(backbutton)
        backbutton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
    }
}

extension SharedController {
    @objc fileprivate func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
