//
//  RegistrationNormalController.swift
//  AuthnexPodTest
//
//  Created by Himal Madhushan on 6/29/19.
//  Copyright © 2019 Authnex. All rights reserved.
//

import UIKit
import AirshipKit
import Authnex

class RegistrationNormalController: SharedController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainTitle.text = "Background Registration Example \nSee Code"
        
        let channelid = UAirship.push()?.channelID
        if channelid != nil {
            registerUser(username: "demouser", mappeduerid: "example@some.com", companyid: 63, pushtoken: channelid!, mobilenumber: "07xxxxxxx")
        } else {
            UAirship.push()?.registrationDelegate = self
        }
    }
}

extension RegistrationNormalController: UARegistrationDelegate {
    func registrationSucceeded(forChannelID channelID: String, deviceToken: String) {
        //MARK: User registration
        if(!AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
            registerUser(username: "demouser", mappeduerid: "example@some.com", companyid: 63, pushtoken: channelID, mobilenumber: "07xxxxxxx")
        }
    }
}

extension RegistrationNormalController {
    fileprivate func registerUser(username: String, mappeduerid: String, companyid: Int, pushtoken: String, mobilenumber: String) {
        
        AuthnexCore.sharedInctance.userRegistrationNormal(userName: username, mappeduserId: mappeduerid, companyId: companyid, pushToken: pushtoken, mobileNumber: mobilenumber) { (success, err) in
            DispatchQueue.main.async {
                if success {
                    let loginviewcontroller = LoginViewController()
                    self.present(loginviewcontroller, animated: true, completion: nil)
                    return
                }
                
                print("not success \(err!)")
            }
        }
    }
}
