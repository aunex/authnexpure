//
//  LoginController.swift
//  AuthnexPodTest
//
//  Created by Himal Madhushan on 6/29/19.
//  Copyright © 2019 Authnex. All rights reserved.
//

import UIKit
import Authnex
import AirshipKit

class LoginViewController: SharedController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainTitle.text = "Login With QR Example \nSee Code"
        userLogin()
    }
}

//MARK: Sample login -> Scan the login qr image and pass the qr codestring here
extension LoginViewController {
    func userLogin() {
        AuthnexCore.sharedInctance.login(with: "270EOo4V8dV+WJ5VjglPYW0uSlJn8Y06Ls4AY1xGTHDP3hp8g24+lhHS0G4X7QVV") { (success, err, code) in
            if success {
                print("user authenticated")
                return
            }
            print("error in user login with qr : \(err!)")
        }
    }
}
