//
//  ViewController.swift
//  AuthnexPodTest
//
//  Created by Himal Madhushan on 6/29/19.
//  Copyright © 2019 Authnex. All rights reserved.
//

import UIKit
import Authnex

class RootViewController: SharedController {
    
    private var menuTableView: UITableView!
    private let menucellIdentifier = "menucellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setMenu()
    }
}

extension RootViewController {
    fileprivate func setMenu() {
        menuTableView = UITableView(frame: .zero, style: .grouped)
        menuTableView.translatesAutoresizingMaskIntoConstraints = false
        menuTableView.register(UITableViewCell.self, forCellReuseIdentifier: menucellIdentifier)
        menuTableView.delegate = self
        menuTableView.dataSource = self
        view.addSubview(menuTableView)
        
        let menutableviewConstraints = [menuTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
                                        menuTableView.topAnchor.constraint(equalTo: view.topAnchor),
                                        menuTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
                                        menuTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)]
        NSLayoutConstraint.activate(menutableviewConstraints)
    }
}

extension RootViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menucell = tableView.dequeueReusableCell(withIdentifier: menucellIdentifier)
        menucell?.textLabel?.font = UIFont(name: "Helvetica-Bold", size: 20 * fontScale)
        switch indexPath.row {
        case 0:
             menucell?.textLabel?.text = "QR Registration Example"
        case 1:
             menucell?.textLabel?.text = "Normal Registration Example"
        case 2:
             menucell?.textLabel?.text = "QRLogin Example"
        case 3:
             menucell?.textLabel?.text = "Approval Example"
        default:
            return menucell!
        }
        return menucell!
    }
}

extension RootViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let fullHeight = deviceHeight - deviceHeight/10
        return fullHeight/4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var viewcontroller: UIViewController!
        
        switch indexPath.row {
        case 0:
            viewcontroller = RegistrationQRController()
        case 1:
            viewcontroller = RegistrationNormalController()
        case 2:
            if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
                viewcontroller = LoginViewController()
            } else {
                showAlert()
                return
            }
        case 3:
            if(AuthnexCore.sharedInctance.isRegisteredToAuthnex()) {
                viewcontroller = ApprovalController()
            } else {
                showAlert()
                return
            }
        default:
            viewcontroller = UIViewController()
        }
        DispatchQueue.main.async {
            self.present(viewcontroller, animated: true, completion: nil)
        }
    }
}

extension RootViewController {
    fileprivate func showAlert() {
        let alertController = UIAlertController(title: "Warning", message: "Please do the registration first", preferredStyle: .alert)
        let cancelaction = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in }
        alertController.addAction(cancelaction)
        self.present(alertController, animated: true, completion: nil)
    }
}

